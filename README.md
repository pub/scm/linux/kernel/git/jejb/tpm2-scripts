Overview
========

This is a set of scripts for testing the Linux TPM driver TPM2 2.0
functionality.

Credits
=======

TPM 2.0 simulator access code is derived from the works of Microsoft
and Peter Hüwe. Otherwise, the code is copyrighted by Intel. This
code is located in the class tpm2.Simulator and tpm2-simulator-vtpm
script.
